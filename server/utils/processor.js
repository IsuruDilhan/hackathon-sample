const regression = require('../helpers/regression')();

const a = 10;
const b = 5;

//utility functions
//can be facade or factory functions
const processor = function() {
	
	function calcTotal() {
		return a + b;
	}

	function calcSubs() {
		return a - b;
	}

	function calcRegression() {
		return regression.data;
	}

	return {
		total: calcTotal,
		subs: calcSubs,
		reg: calcRegression
	};
};

module.exports = processor;