const reg = ['some', 'data', 'within', 'an', 'array'];

//helper function for small tasks
//modular in nature
//revealing modular pattern
const regressionHelper = function() {
	return {
		data: reg
	}
};

module.exports = regressionHelper;
