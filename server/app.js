const express = require('express');
const app = express();
const mongoose = require('mongoose');
const PORT = process.env.PORT || 3000;
const router = express.Router();
const indexRoutes = require('./routes/index');
const mongooseCreds = require('./config/mongoose');

mongoose.connect(mongooseCreds);

app.use(router);

app.use('/', express.static('src'));
indexRoutes(router);

app.listen(PORT);

module.exports.getApp = app;