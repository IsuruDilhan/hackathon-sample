const express = require('express');
const processor = require('../utils/processor')();

const indexRoutes = function(router) {
	
	router.get('/total', function(req, res) {
		//processing
		res.json({
			total: processor.total()
		});
	});

	router.get('/reg', function(req, res) {
		//processing
		res.json({
			total: processor.reg()
		});
	});	

}

module.exports = indexRoutes;